<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'logos_roles_description' => 'Remplacer le système de logos de SPIP par un système basé sur le plugin « Rôles de documents ­».',
	'logos_roles_nom' => 'Logos par rôle',
	'logos_roles_slogan' => 'Un système de gestion des logos plus flexible',
);
